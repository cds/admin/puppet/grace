# Class: grace
# ===========================
#
# Full description of class grace here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'grace':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class grace (
    String $package_name,
    String $module_filestore,
    String $font_dir,
){

    if $facts['os']['family'] == 'RedHat' {
	package {'grace_package':
	    name   => $package_name,
	    ensure => installed,
	}
	if $facts['os']['release']['major'] == '7' {
	    package {'urw-base35-fonts-legacy':
		ensure => installed,
	    }	
	    file {'/usr/share/grace/fonts/type1':
		ensure => link,
		target => $font_dir,
		require => Package['grace_package', 'urw-base35-fonts-legacy'],
	    }
	}
	file { '/etc/grace/gracerc':
	    ensure  => file,
	    mode    => '0644',
	    owner   => 'root',
	    group   => 'root',
	    source  => "${module_filestore}/etc/grace/gracerc",
	    require => Package['grace_package'],
	}
    }
}
